<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSensorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sensors', function (Blueprint $table) {
            $table->string('sensor_id' , 30)->primary();
            $table->string('zone_id');
            $table->string('node_id');
            $table->string('sensor_location');
            $table->string('survey_point');
            $table->string('sensor_type');
            $table->string('longitude_x');
            $table->string('latitude_y');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sensors');
    }
}
