<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('links', function (Blueprint $table) {
            $table->string('link_id' , 30)->primary();
            $table->string('zone_id');
            $table->string('barangay_id');
            $table->string('link_name');
            $table->string('link_class');
            $table->string('func_class');
            $table->string('A_node');
            $table->string('B_node');
            $table->string('longitude_x');
            $table->string('latitude_y');
            $table->string('elevation');
            $table->string('num_lanes');
            $table->string('n_laneset');
            $table->string('lane_width');
            $table->string('lane_cap');
            $table->string('cwaywidth');
            $table->string('right_way');
            $table->string('oneway');
            $table->string('link_surface');
            $table->string('link_condition');
            $table->string('link_terrain');
            $table->string('distance');
            $table->string('max_speed');
            $table->string('total_capacity');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('links');
    }
}
