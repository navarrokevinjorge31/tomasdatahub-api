<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use DB;
use Validator;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function showtables(){
        
        $database = Config('database.connections.mysql.database');
        $table_schema = DB::select("SELECT TABLE_NAME,COLUMN_NAME,COLUMN_COMMENT FROM information_schema.COLUMNS WHERE table_schema = '".$database."' and TABLE_NAME in ('dim_links','dim_nodes','dim_od_matrices','dim_sensors','dim_sensor_count','dim_signal_timing','dim_socio_dgraphic_data','dim_zones','fct_sensorcount_perday','fct_sensorcount_perweek','fct_sensorcount_permonth','fct_sensorcount_peryear');");
        $table_name = [];
        foreach ($table_schema as $tables) {
            $table_names = $tables->TABLE_NAME;
            $column = $tables->COLUMN_NAME;
            $comments = $tables->COLUMN_COMMENT;
            if ($comments == "") {
               $comments = "null";
            }
            foreach ($tables as $columns) {
                $table_name[$table_names][$column] = $comments; 
           }
        }
    
        return response()->json(['message' => "Successful retrieving data", 
                                'data' => $table_name,
                                'error' => false,
                                'error_code' => 200,
                                'line'    => "line ".__LINE__." ".basename(__FILE__)], 200);

    }


    public function showData(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'columns' => 'required',
            'table_name' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json(['message' => implode(" ",$validator->messages()->all()), 
                                 'error' => true,
                                 'error_code' => 400,
                                 'line'    => "line ".__LINE__." ".basename(__FILE__)], 200);
        }else{

            
            $columns = $request->columns;
            $table_name = $request->table_name;
            $n_columns = explode(",", $columns);
                $data = 
                DB::table($table_name)
                ->select($n_columns)
                ->get();
                return $data;

        }
    }

    public function mvCounts(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'flag' => 'required'
        ]);

        if($validator->fails()) {
            return response()->json(['message' => implode(" ",$validator->messages()->all()), 
                                 'error' => true,
                                 'error_code' => 400,
                                 'line'    => "line ".__LINE__." ".basename(__FILE__)], 200);
        }else{

                $data = DB::select(DB::raw("Call myCounts :Param1"),[
                    ':Param1' => $request,
                ]);
                return $data;

        }
    }

    function getTables()
    {
        $tables = DB::select('SHOW TABLES');
        $tables = array_map('current',$tables);
        return $tables;
    }

    public function getData(Request $request , $tablename){
        $data['request'] = DB::table($tablename)->get();
        return $data;
      }   
    
}
