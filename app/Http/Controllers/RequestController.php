<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RequestModel;
use Validator;
use Redirect;

class RequestController extends Controller
{
    public function create_request (Request $request){

        $validator = Validator::make($request->all(),[
           'user_id' => 'required',
           'data' => 'required',
           'status' => 'required',
       ]);  
       if ($validator->fails()) {
           return response()->json(['message' => implode(", ",$validator->messages()->all()), 
                                    'error' => true,
                                    'error_code' => 400,
                                    'line'    => "line ".__LINE__." ".basename(__FILE__)], 200);
       }
       $data = $request->all();
       $createRequest = RequestModel::create($data);
       return response()->json(['message' => "Successfully Create Request", 
                                'error' => false,
                                'error_code' => 200,
                                'line'    => "line ".__LINE__." ".basename(__FILE__)], 200); 
   }

   public function getRequest(Request $request , $id){
    $data['request'] = RequestModel::where('user_id', $id)->get();
    return $data;
  }   

  // public function deleteRequest(Request $request , $id){
  //     RequestModel::where('request_id',$id)->delete();
  //     return Redirect::to('request')->with('success','Product deleted successfully');
  // }

  public function delete_request(Request $request){

    $validator = Validator::make($request->all(),[
       'request_id' => 'required',
   ]);  
   if ($validator->fails()) {
       return response()->json(['message' => implode(", ",$validator->messages()->all()), 
                                'error' => true,
                                'error_code' => 400,
                                'line'    => "line ".__LINE__." ".basename(__FILE__)], 200);
   }
   $data = $request->all();
   $createRequest =  RequestModel::where('request_id',$data)->where('user_id',$data)->delete();
   return response()->json(['message' => "Successfully Delete Request", 
                            'error' => false,
                            'error_code' => 200,
                            'line'    => "line ".__LINE__." ".basename(__FILE__)], 200); 
    }   

    public function update_request(Request $request){

    $validator = Validator::make($request->all(),[
        'request_id' => 'required',
        'status' => 'required',
    ]);

    }
}
