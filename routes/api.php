<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'v1'],function(){
    Route::get('/tables','Controller@showtables');
    Route::post('/showData','Controller@showData');
    Route::post('/mvCounts','Controller@mvCounts');
    Route::get('/get','Controller@getTables');
    Route::get('/data/{tablename}','Controller@getData');
    Route::get('foo', function () {
        return 'Test';
    });
    Route::post('/create','RequestController@create_request');
    Route::get('/myrequest/{id}','RequestController@getRequest');
    Route::post('/cancel','RequestController@delete_request');
});


